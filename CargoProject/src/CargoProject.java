



import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

 

 
public class CargoProject {
	enum GovtHoliday{
		JANUARY_1(LocalDate.of(2021, 1, 1)),
		 JANUARY_26(LocalDate.of(2021, 1, 26)),
		 AUGUST_15(LocalDate.of(2021, 8, 15)),
		 DECEMBER_25(LocalDate.of(2021,12,25));
		private LocalDate date;
		 private GovtHoliday(LocalDate date) {
			 this.date = date;
		 }
		 public LocalDate get() {
			 return date;
		 }
	}
	enum Weekend{
		SATURDAY("Saturday"),
		SUNDAY("Sunday");
		private String day;
		public String getDay() {
			return day;
		}
		private Weekend(String day) {
			this.day=day;
		}
		 
	}
	enum ShipmentDetails{
		  
		STARTHOUR(6);
		 private int d;
		 private ShipmentDetails(int i) {
			 this.d = i;
		 }
		 public int get() {
			 return d;
		 }		
		
	}
	public void CalcCargoTime() {
		LocalDate ld = LocalDate.now();
		System.out.println(ld);

	   System.out.println("Enter Duration of Shipment");
	   Scanner sc = new Scanner(System.in);
	   int sd=sc.nextInt();
	   
		Month mo = ld.getMonth();
		int date = ld.getDayOfMonth();
		int year = ld.getYear();
		 
		System.out.println("SHIPMENT START DATE  "+date+" "+mo+" "+year);
		
	 	int totalduration = sd ;
		ShipmentDetails sds = ShipmentDetails.STARTHOUR;
		int starttime = sds.get();
			
		while(totalduration>12) {
				for(Weekend w: Weekend.values()) {
				DayOfWeek dayOfWeek = ld.getDayOfWeek();
				String displayName = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
				if(w.getDay().equals(displayName))
				{
					ld = ld.plus(1,ChronoUnit.DAYS);
					continue;
				}
			}
			for(GovtHoliday gh : GovtHoliday.values())
			{
				
				if(gh.get().compareTo(ld)==0)
				{
					ld = ld.plus(1,ChronoUnit.DAYS);
					continue;
				}
			}
			totalduration -=12;
			ld = ld.plus(1,ChronoUnit.DAYS);
			
		}
		
		String time;
		int checker = (starttime+totalduration)%12;
		if(checker>6 && checker<12) {
			 time = "am";
		}
		else
		{
			time = "pm";
		}
			System.out.println("The ship will reach on "+ld+" at "+checker+" "+time+"  "  );
	}
	
public static void main(String[] args) {
	CargoProject cp=new CargoProject();
	cp.CalcCargoTime();
}
}
