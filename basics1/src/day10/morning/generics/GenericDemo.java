package day10.morning.generics;

import java.util.ArrayList;

//wrong code
//paint brush is generic and not specific to paint
public class GenericDemo {
public static void main(String[] args) {
	PaintBrush <Paint> pb=new PaintBrush<>();
	pb.setObj(new RedPaint());
	Paint paint=pb.getObj();
	paint.doColour();
	 
	ArrayList<String> a2=new ArrayList<>();
	ArrayList<Integer> a1=new ArrayList<>();
	a1.add(3444);
	a1.add(2333);
	a1.add(1223);
	for(int i:a1) {
		System.out.println(i);
	}
}
}
class PaintBrush<T>{
	private T obj;

	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
}
 
abstract class Paint{
	abstract void doColour();

}
class RedPaint extends Paint{
	@Override
	void doColour() {
      System.out.println("Red Colour");		
	}
	
}
abstract class Liquid{
	abstract void spray();
}  
class ColourLiquid extends Liquid{
	@Override
	void spray() {
System.out.println("holi holie");		
	}
}

abstract class Dry{
	abstract void gijiGija();
	abstract void dusting();
}

class DryAir extends Dry{
	@Override
	void dusting() {
System.out.println("I clean my laptop..key board");		
	}

	@Override
	void gijiGija() {
System.out.println("I play tickle");		
	}
}