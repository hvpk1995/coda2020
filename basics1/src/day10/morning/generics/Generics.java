package day10.morning.generics;
//wrong code
//paint brush is generic and not specific to paint
public class Generics {
public static void main(String[] args) {
	PaintBrusha pb=new PaintBrusha();
	pb.paint=new RedPainta();
	pb.doPaint();
}
}
class PaintBrusha{
	Painta paint;
	public void doPaint() {
		paint.doColour();
	}

}
abstract class Painta{
	abstract void doColour();

}
class RedPainta extends Painta{

	@Override
	void doColour() {
      System.out.println("Red Colour");		
	}
	
}
  