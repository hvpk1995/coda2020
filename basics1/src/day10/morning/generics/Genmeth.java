package day10.morning.generics;

public class Genmeth {
	static <T> void genericDisplay(T element)
	{
		System.out.println(element.getClass().getName()+"="+element);
	}
	public static void main(String[] args) {
	genericDisplay(11);
	genericDisplay("Google the question");
	genericDisplay(1.0);
}
}
