package Revision;

import java.util.Set;

public class GarbageCollectionTest  {

	public static void main(String[] args) throws Exception {
		Runtime r=Runtime.getRuntime();
		System.out.println("Before Death"+r.freeMemory());
		GrandFather tt=new GrandFather();
		System.out.println("After Death "+r.freeMemory());
		tt=null;
		r.gc();
		System.out.println("After Clearing"+r.freeMemory());
		//System.out.println(tt.age);
		
		Set<Thread> threads = Thread.getAllStackTraces().keySet();
		System.out.println(threads); 
	}
}
class GrandFather{
	String age;
	private String secret="Hidden under tree";
	public GrandFather() {
		for (int i = 0; i <1000; i++) {
			age=new String(i+"");
		}
		
	}
	private String getGold() {
	return "Gold is"+secret;
	}
	protected void finalize() throws   Throwable {
		System.out.println("After Death"+getGold());
	}
}