package Revision;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
public class InterfaceMohan {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AlopathyCollege kmc = new AlopathyCollege();
		Airways pilot = new Airways();
		
		Human dora = new Human();
		
		Object o = Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class}, new MyInvocationHandler(new Object[] {kmc,pilot}));
		
		Doctor doraDoc = (Doctor)o;
		Pilot doraPilot = (Pilot)o;
		
		doraDoc.doCure();
		doraPilot.doFly();
	}
}
class MyInvocationHandler implements InvocationHandler{
	Object obj[];
	Object o;
	public MyInvocationHandler(Object[] obj) {
		// TODO Auto-generated constructor stub
		this.obj = obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		for(Object p: obj) {
			try {
				o = method.invoke(p, args);
				return o;
			}catch(Exception e) {
//				return o;
			}
		}
		return null;
	}
}
class Human 	{}
interface Doctor{
	public void doCure();
}
interface Nurse{
	public void doTreatment();
}
interface Pilot{
	public void doFly();
}
interface Rescue{
	public void doJump();
}
class AlopathyCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		// TODO Auto-generated method stub
		System.out.println("I do surgery...");
	}
	@Override
	public void doTreatment() {
		// TODO Auto-generated method stub
		System.out.println("I do treatment");
	}
}
class Airways implements Pilot, Rescue{
	@Override
	public void doFly() {
		// TODO Auto-generated method stub
		System.out.println("I fly plane");
	}
	@Override
	public void doJump() {
		// TODO Auto-generated method stub
		System.out.println("I throw passengers");
	}
}