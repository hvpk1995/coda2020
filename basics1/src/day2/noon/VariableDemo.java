package day2.noon;

public class VariableDemo {
	public static void main (String[] args) {
		String str="Hello World";
		byte b=20;
		String str2= new String("Hello World 2");
		char c='a';
		char cc=99;
		int i=1000;
		long lo=1010101011;
		float f =1.3f;
		double d=1.3333;
		double dd=f;
		boolean boo=true;
		int j=new Integer (30);//auto unboxing
		Integer bla=new Integer(32);
		Integer bla2=i;//autoboxing
		
		//Wrapper Classes
		Character ch=new Character('c');
		Double dob=new Double(202020.22);
		Long lon=new Long(212121);
		Integer in=new Integer(1220);
		Boolean boo1=Boolean.FALSE;
		
		//Type Casting
		int num=b;
		byte byt=(byte)num; //When you do higher to lower casting you lose the precision
		
		//How to convert a string to number
		String strnum="2020";
		int numm=Integer.parseInt(strnum);
		
		
	}

}
