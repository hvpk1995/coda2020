package day2.noon;

public class ArrayDemo {
	public static void main(String[] args) {
		//quick revision of Arrays.
	
		//declarations
		int ar[]= {1,2,3,4,5};
		int arr[]=new int[] {1,2,3,4,5};
		int []a=new int[5];
		String sar[]=new String[] {"ramu","somu"};
		//complex type array
		Laddu laddutray[]= {new Laddu(),new Laddu(),new Laddu()};
		
		for(int i:ar) {
			System.out.println(i);
		}
		for(String s:sar) {
			System.out.println(s);
		}
		for(Laddu laddu:laddutray) {
			System.out.println(laddu);
		}
		
		//Two Dimensional Array
		int ar2[][]= {{1,2,3,4},{5,6,7,8},{10,11,12}};
		int arr2[][]=new int[][] {{1,2,3,4},{5,6,7,8},{10,11,12}};
		int rows=4;int cols=6;
		int [][]a2=new int[rows][cols];
		String sar2[][]=new String[][]{{"aa","bb"},{"cc","dd"}};
		Laddu laddutray2[][]=new Laddu[2][3];
		laddutray2[0][0]=new Laddu();
		laddutray2[0][1]=new Laddu();
		laddutray2[0][2]=new Laddu();
		laddutray2[1][0]=new Laddu();
		laddutray2[1][1]=new Laddu();
		laddutray2[1][2]=new Laddu();
		for(Laddu l[]:laddutray2) {
			for(Laddu ll:l) {
				System.out.print(ll+"\t");
			}
			System.out.println();
		}		
	}	
}

class Laddu{}