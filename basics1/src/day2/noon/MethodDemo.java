package day2.noon;
import org.junit.Test;
import static org.junit.Assert.*;

public class MethodDemo {
//Where you write business logic
//Basics of methods
	//can be void or can have a return type
	//Methods can have parameters
	//they can have access modifiers
	//It always have a body.body defines the scope
	//name should always start with lower case
	//Inside method you write logic
public void net1(String s) {
	if(s.equals("ramu")) {
			
	System.out.println("The Value is ramu..");
	}
	else if(s.equals("sonu")) {
		System.out.println("The Value is sonu..");
		
	}
	else {
		System.out.println("DEFAULT VALUE IS..."+s);
	}
	boolean b=true;
	if(b) {
		
	}
	int j=10;
	if(j==10) {
		
	};
	char c='a';
	if(c=='a') {
		
	}
	//for loop
	for(int i=0;i<5;i++) {
		System.out.println("value of i..."+i);
		
	}
	while(j>5) {
		j--;
		System.out.println("Value of j is"+j);
	}
	do 
	{
		j--;
		System.out.println("Value of j in do while is .."+j);
		
	}while(j>5);
}
int net2() {
	return 1;
}
public static void main(String[] args) {
	MethodDemo obj=new MethodDemo();
	//obj is reference
	//MethodDemo is class
	obj.net1("ramu");
}
}
