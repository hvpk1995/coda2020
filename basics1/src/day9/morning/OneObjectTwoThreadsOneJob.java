package day9.morning;

public class OneObjectTwoThreadsOneJob {
public static void main(String[] args) {
	Gun bofors=new Gun();
	new Thread(()->{
		for (int i = 0; i < 5; i++) {
			bofors.fill();
		}
	},"filler").start();
	
new Thread(()->{
	for (int i = 0; i < 5; i++) {
		bofors.shoot();
	}
		 
	},"shooter").start();
}
}
//add synchonized so fill gun and shoot gun dont execute five times in a row
//wait method is not time bound.until you notify thread will be in wait mode
//wait only in synchronized.sleep always time bound
//wait also has time bound
//when you have wait method another thread cannot enter monitor(synchronized)
//two threads are talking to each other..
//inter thread communication

//Dead Lock
//when two synchronized blocks talk to each other or when their is a cirular interdependency between two synchronized blocks, dead lock occurs.
class Gun{
	boolean flag;
	//by default flag is false
	synchronized public void fill() {
		
		if(flag) {
			try {wait();}catch(Exception e) {}
		}
		System.out.println("fill the gun...");
	   flag=true;
	   notify();
	}
	synchronized public void shoot() {
		if(!flag) {
			try {wait();}catch(Exception e) {}
		}
		System.out.println("shoot the gun...");
	   flag=false;
	   notify();
	}
	
}