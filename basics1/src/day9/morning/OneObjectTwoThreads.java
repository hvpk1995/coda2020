package day9.morning;

public class OneObjectTwoThreads {
public static void main(String[] args) throws InterruptedException {
	ReservationCounter counter =new ReservationCounter();
	new Thread(()->{
		counter.bookTicket(1000);
		counter.giveChange();
	},"ramu").start();//ramu name of thread //getName() returns name of thread
	//Thread.sleep(1);
	
	//thread1 is created sits in waiting
	//main thread completes
	//then after that any of the thread comes out
	//solution is isolation...
	//isolation is not always same.isolation differs based on severity .two types:
	//1.Pessimistic
	//2.Optimistic
	//we want to make use of synchronised
	//synchronised keyword brings thread safety.if it is given only one keyword is active in that method.until it completes the task another cannot enter the thread
	//after using synchronized on givechange out put is not correct if we rerun the code
	new Thread(()->{
		//counter.bookTicket(500);
		//counter.giveChange();
		counter.drinkWater();
	},"somu").start();
			

}
}
class ReservationCounter{
	int amt;
	synchronized public void bookTicket(int amt) {
		this.amt=amt;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Ticket Booked for..:"+name+ "   and amount paid is..:"+amt);
	 try {Thread.sleep(10000);
	 }catch(Exception e){}
	 }
	synchronized public void giveChange() {
		
		int change=amt-100;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Change given to..."+name+"  and change given is.."+change);
	}
	
	  public void drinkWater() {
		System.out.println("drinking water");
	}
}