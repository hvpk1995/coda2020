package day9.morning;

public class DeadLockDemo {
public static void main(String[] args) {
	Frog frog=new Frog();
	Crane crane=new Crane();
	
	new Thread(()->{
		crane.eat(frog);
	}).start();
	new Thread(()->{
		frog.escape(crane);
	}).start();
}
}
class Frog{
	synchronized public void escape(Crane c) {
		c.cranesLeaveMethod();
		//System.out.println("Freedom..");
	}
	synchronized public void frogsLeaveMethod() {
		//System.out.println("let go");
	}
}
class Crane{
	synchronized public void eat(Frog f) {
		System.out.println("Eating..");
		System.out.println("Eating..");
		f.frogsLeaveMethod();
		System.out.println("swaha");
	}
	synchronized public void cranesLeaveMethod() {
		
	}
}