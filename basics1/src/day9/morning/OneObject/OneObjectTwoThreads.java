package day9.morning.OneObject;

public class OneObjectTwoThreads {
public static void main(String[] args) throws InterruptedException {
	ReservationCounter counter =new ReservationCounter();
	new Thread(()->{
		synchronized(counter) {
		counter.bookTicket(1000);
		counter.giveChange();}
	},"ramu").start(); 
	new Thread(()->{
		synchronized(counter) {
		 counter.bookTicket(500);
		 counter.giveChange();
		}//counter.drinkWater();
	},"somu").start();
			

}
}
class ReservationCounter{
	int amt;
	public void bookTicket(int amt) {
		this.amt=amt;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Ticket Booked for..:"+name+ "   and amount paid is..:"+amt);
//	 try {Thread.sleep(10000);
//	 }catch(Exception e){}
	 }
	  public void giveChange() {
		
		int change=amt-100;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Change given to..."+name+"  and change given is.."+change);
	}
	
	  public void drinkWater() {
		System.out.println("drinking water");
	}
}