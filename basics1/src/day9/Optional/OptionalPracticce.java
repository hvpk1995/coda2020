package day9.Optional;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream .*;
public class OptionalPracticce {
	
public static void main(String[] args) {
	//String str1 = null; 
	//System.out.println("Enter name");
	// Scanner sc=new Scanner(System.in);
	//str1= sc.next();
	//isPresent( str1);
int n=10;
	 
//	System.out.println(str1.isEmpty());

	 String s[]=new String[5];
	 s[0]="Ramu";
	 s[1]="Somu";
	 Optional<Integer> o= Optional.ofNullable(n);
	 Optional<String> o1= Optional.ofNullable(s[1]);
	// Optional<String> o4= Optional.of (s[2]);
	 Optional<Integer> op1  = Optional.of(456); 
   Optional<Integer> op2 = Optional.of(456); 
   System.out.println("Comparing Optional 1  and Optional 2: " + op1.equals(op2)); 
	 Optional<String> o2= Optional.ofNullable(s[2]);
	 Optional<String> o3= Optional.ofNullable(s[0]);
	//A container object which may or may not contain a non-null value. If a value is present, isPresent() will return true and get() will return the value.
	 System.out.println(o1.isPresent());
	//isempty
	System.out.println(o2.empty());
	System.out.println(o3.empty());
	//hashcode
	System.out.println(o2.hashCode());
	System.out.println(o1.hashCode());
	 System.out.println(o);
	 System.out.println(o1);
	 System.out.println(o2);
	 //orElse
	 String nullName = null;
	    String name = Optional.ofNullable(nullName).orElse("john");
	    System.out.println(name);
	  //orElseGet
	    String nullName2 = null;
	    String name2 = Optional.ofNullable(nullName).orElseGet(() -> "john j");
	    System.out.println(name2);
	    
	    //orElseThrow
	    String nullName3 = "Ram";
	    String name3 = Optional.ofNullable(nullName3).orElseThrow(
	      IllegalArgumentException::new);
	    System.out.println(name3);
	  
	    //filter
	    Integer year = 2016;
	    Optional<Integer> yearOptional = Optional.of(year);
	    boolean is2016 = yearOptional.filter(y -> y == 2016).isPresent();
        System.out.println(is2016);
	    boolean is2017 = yearOptional.filter(y -> y == 2017).isPresent();
	    System.out.println(is2017);

	    //map
	    String name4 = "javat";
	    Optional<String> nameOptional = Optional.of(name4);
	 	    int len = nameOptional
	     .map(String::length)
	     .orElse(0);
	 	    System.out.println(len);
	 	    
	 	    //get
	 	   Optional<String> opt = Optional.of("baeldung");
	 	    String name5 = opt.get();
	 	    System.out.println(name5);
	 	
	  //tostring
	 	   // create a Optional 
	        Optional<Integer> op 
	            = Optional.of(452146); 
	  
	        // get value using toString 
	        String value = op.toString(); 
	  
	        // print value 
	        System.out.println("String Representation: "
	                           + value); 
	 //tostring empty
	     // create a Optional 
	        Optional<Integer> op11 
	            = Optional.empty(); 
	  
	        // get value using toString 
	        String value1 = op.toString(); 
	  
	        // print value 
	        System.out.println("String Representation: "
	                           + value1); 
	        
}
}
class OptionalDemo{
	private Optional<String> getEmpty() {
	    return Optional.empty();
	}
	 
	private Optional<String> getHello() {
	    return Optional.of("hello");
	}
	 
	private Optional<String> getBye() {
	    return Optional.of("bye");
	}
	 
	private Optional<String> createOptional(String input) {
	    if (input == null || "".equals(input) || "empty".equals(input)) {
	        return Optional.empty();
	    }
	    return Optional.of(input);
	}
	
	public void givenThreeOptionals_whenChaining_thenFirstNonEmptyIsReturned() {
	    Optional<String> found = Stream.of(getEmpty(), getHello(), getBye())
	      .filter(Optional::isPresent)
	      .map(Optional::get)
	      .findFirst();
	    
	     System.out.println(found);
	}
	
}

// Every Java Programmer is familiar with //NullPointerException. It can crash your code. And it is very //hard to avoid it without using too many null checks.
//Java 8 has introduced a new class Optional in java.util //package. It can help in writing a neat code without using //too many null checks. By using Optional, we can specify //alternate values to return or alternate code to run.
//Optional is a container object which may or may not contain a non-null value. 
//notes
//empty() is used to get an empty instance of this Optional //class
//isnullabe()is used to get an instance of this Optional class //with the specified value of the specified type. If the //specified value is null, then this method returns an empty //instance of the Optional class
//to string Optional class in Java is used to get the string //representation of this Optional instance
