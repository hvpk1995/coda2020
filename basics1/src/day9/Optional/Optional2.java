package day9.Optional;

import java.util.Optional;

public class Optional2 {
public static void main(String[] args) {
	Optional<Object> empty=Optional.empty();
	System.out.println(empty.isPresent());
	
	Optional <String> name1=Optional.of("Hello");
	name1.ifPresent(word->{
		System.out.println(word);
	});
	
//	Optional <String> name2=Optional.of(null);
//	name1.ifPresent(word2->{
//		System.out.println(word2);
//	});
	
	//jdk 9 if present or else
}
}
