package day9.Optional;

import java.util.Optional;

public class Optional3 {
public static void main(String[] args) {
	//Person person=new Person("james","JAMES@gmail.com");
	Person person=new Person("james",null);
	System.out.println(person.getEmail().map(String::toLowerCase ).orElse("Email not Found"));
	
}
}
class Person{
	private String name;
	private String email;
	public Person(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	 
//	public String getEmail() {
//		return email;
//	}
	 public Optional<String> getEmail(){
		 return Optional.ofNullable(email);
	 }
	
	 
}