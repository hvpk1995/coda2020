package day9.practice;

public class DeadLock{
	public static void main(String[] args) {
		Struggle strg=new Struggle();
		new Thread(()->{
			synchronized(strg) {
				strg.eat(1);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 System.out.println("Crane tries to eat.."); 
				
				}
			}
			).start();
	
		new Thread(()->{
			synchronized(strg) {
				strg.eat(2);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 System.out.println("Frog tries to escape by holding crane's neck.."); 
				
				}
			}
			).start();
	 
	}
	}
class Struggle{
 	  void eat(final int food) {
 		  //food=2;
 		   
		System.out.println(food);
	}
}