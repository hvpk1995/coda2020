package day9.executorserviceconvert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneThreadOneObjectEs {

	
		public static void main(String[] args) {
			ExecutorService es=Executors.newFixedThreadPool(2);
			es.execute(()->{
				Resource r=Factory.getResource();
				r.name="This is the First Thread Object";
				Resource r2=Factory.getResource();
				System.out.println(r2.name);
				// r=Factory.getResource();
				// r=Factory.getResource();
				 Resource r3=Factory.getResource();
			 //  Resource r4=Factory.getResource();
			  // Resource r5=Factory.getResource();
			   System.out.println(r3.name);
			   //System.out.println(r4.name);
			   Factory.removeResourceFromThread();
			   Resource r6=Factory.getResource();
			  // r6.name="after deleting thread";
			   //System.out.println(r6.name);
			});
			//thread id will be there.based on that object is created and cons called
			 es.execute(()->{
				 Resource resource=Factory.getResource();
				 resource.name="Second Thread Object";
				 System.out.println("Second Thread.."+resource.name);
				 
				 resource=Factory.getResource();
			 }) ;
			
		}
		}

		class Factory{
			private static ThreadLocal tlocal=new ThreadLocal();
			
			public static Resource getResource() {
				Resource r=(Resource)tlocal.get();
				if(r==null) {
					r=new Resource();
					tlocal.set(r);
					return r;
				}else {
					return r;
				}
		 	}
			
			public static void removeResourceFromThread() {
				if(tlocal.get()!=null) {
					tlocal.remove();
				}
			}
		}

		class Resource{
			String name;
			public Resource() {
			System.out.println("Resource Constructor called");
			}
		}