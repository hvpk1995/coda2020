package day9.executorserviceconvert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDemo3Es {

	public static void main(String[] args) throws Exception {
		ExecutorService es=Executors.newFixedThreadPool(1);
		//System.out.println("first line");
		//Thread.sleep(5000);
		es.execute(()->{new Test().met();}) ;
		System.out.println("Secone line");
		
	}
	}
	class Test{
		public void met()    {
			System.out.println("first line..");
			try {
			Thread.sleep(5000);}
			catch (Exception e) {}
		}
	}
