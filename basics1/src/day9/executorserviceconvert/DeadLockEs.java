package day9.executorserviceconvert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DeadLockEs {

	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2);
		Frog frog = new Frog();
		Crane crane = new Crane();

		es.execute(() -> {
			crane.eat(frog);
		});
		es.execute(() -> {
			frog.escape(crane);
		});
	}
}

class Frog {
	synchronized public void escape(Crane c) {
		c.cranesLeaveMethod();
		// System.out.println("Freedom..");
	}

	synchronized public void frogsLeaveMethod() {
		// System.out.println("let go");
	}
}

class Crane {
	synchronized public void eat(Frog f) {
		System.out.println("Eating..");
		System.out.println("Eating..");
		f.frogsLeaveMethod();
		System.out.println("swaha");
	}

	synchronized public void cranesLeaveMethod() {

	}
}
