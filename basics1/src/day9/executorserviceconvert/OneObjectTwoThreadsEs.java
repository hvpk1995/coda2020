package day9.executorserviceconvert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

 
public class OneObjectTwoThreadsEs {
	public static void main(String[] args) {
		ThreadFactory threadFactory = new ThreadFactory() {
			 private final AtomicLong threadIndex = new AtomicLong(0);

			   @Override
			   public Thread newThread(Runnable runnable) {
			       Thread thread = new Thread(runnable);
			 
			       thread.setName("ramu" + threadIndex.getAndIncrement());
			       thread.setName("somu"+ threadIndex.getAndIncrement());
			       return thread;
			   }
			};
		ExecutorService es=Executors.newFixedThreadPool(2,threadFactory);	
		ReservationCounter counter =new ReservationCounter();
		
		es.execute(()->{
			counter.bookTicket(1000);
			counter.giveChange();
		}  ) ;
		
		es.execute(()->{
			//counter.bookTicket(500);
			//counter.giveChange();
			counter.drinkWater();
		} ) ;
		
	}
	
}
class ReservationCounter{
	int amt;
	synchronized public void bookTicket(int amt) {
		this.amt=amt;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Ticket Booked for..:"+name+ "   and amount paid is..:"+amt);
	 try {Thread.sleep(1000);
	 }catch(Exception e){}
	 }
	synchronized public void giveChange() {
		
		int change=amt-100;
		Thread t=Thread.currentThread();
		String name=t.getName();
		System.out.println("Change given to..."+name+"  and change given is.."+change);
	}
	
	  public void drinkWater() {
		System.out.println("drinking water");
	}
}