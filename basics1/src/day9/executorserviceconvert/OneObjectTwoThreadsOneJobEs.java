package day9.executorserviceconvert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public class OneObjectTwoThreadsOneJobEs {
	public static void main(String[] args) {
		ThreadFactory threadFactory = new ThreadFactory() {
			  

			   @Override
			   public Thread newThread(Runnable runnable) {
			       Thread thread = new Thread(runnable);
			 
			       thread.setName("fill" );
			       thread.setName("shoot");
			       return thread;
			   }
			};
		ExecutorService es=Executors.newFixedThreadPool(2,threadFactory);
		Gun bofors=new Gun();
		
		
		es.execute(()->{
			for (int i = 0; i < 5; i++) {
				bofors.fill();
			}
		} ) ;
		
	es.execute(()->{
		for (int i = 0; i < 5; i++) {
			bofors.shoot();
		}
			 
		} ) ;
	}
	}
	 
	class Gun{
		boolean flag;
		//by default flag is false
		synchronized public void fill() {
			
			if(flag) {
				try {wait();}catch(Exception e) {}
			}
			System.out.println("fill the gun...");
		   flag=true;
		   notify();
		}
		synchronized public void shoot() {
			if(!flag) {
				try {wait();}catch(Exception e) {}
			}
			System.out.println("shoot the gun...");
		   flag=false;
		   notify();
		}
		
	}
