package day9.noon;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {
public static void main(String[] args) throws Exception {
	ExecutorService es=Executors.newFixedThreadPool(3);
	
	//runnable object is lambda expression
	es.execute(()->{
		System.out.println("Child Thread..");
	});
	 es.execute(new MyRunnableJob());
	
	//runnable void method
	//callable returns object of type future
	//System.out.println("Main Thread..");
	 
	// es.submit(); accepts callable
	
//	Future future=es.submit(()->{
//	return "hello";	
//	});
	 
	 Future future=es.submit(new MyCallableJob());
	System.out.println("Value Returned.."+ future.get());
	
	System.out.println("Main Thread...");
	es.shutdown();
	
}
}

class MyRunnableJob implements Runnable{
	@Override
	public void run() {
System.out.println("I am void void");		
	}
}
class MyCallableJob implements Callable{
	 
	@Override
	public Object call() throws Exception {
		try {Thread.sleep(5000);}catch(Exception e){}
		return "hello";
	}
}