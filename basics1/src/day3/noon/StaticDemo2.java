package day3.noon;

public class StaticDemo2 {
public static void main(String[] args) {
	MuttonShop.sellMutton();
	MuttonShop.sellMutton();
	MuttonShop.sellMutton();
	MuttonShop.sellMutton();
	MuttonShop.sellMutton();
    MuttonShop.met();
    MuttonShop.met();
    MuttonShop.met();
    
}
}
class MuttonShop{
	private MuttonShop() {
		
	}
	static {
		System.out.println("buy meat or make meat...bla bla..");
	}
	public static void sellMutton() {
		System.out.println("Mutton Sold...");
	}
	public static void met() {
		System.out.println("Customer Arrived...");
	}
}