package day3.noon;

public class OverrideDemo {
	public static void main(String[] args) {
		Parent parent=new Child();
		parent.met();
	}

}
class Parent{
	Parent(){
		System.out.println("Non parameter constructor");
	}
	Parent(int i){
	System.out.println("Parent constructor called..");
	}
	  void met() {
		System.out.print("Method of parent called...");
	}

}

class Child extends Parent{
	Child(){
		//super();
		 super(23);//this should be first line in constructor
		System.out.println("Child constructor called..");
	}
 protected void met() {
	 super.met();
	 System.out.println("Child met method called....");
 }
}

class GrandChild extends Child{
	protected void met() {
		super.met();
	}
}
//class Child extends Parent{
//	 public int met() {
//		 super.met();
//		 System.out.println("Child met method called....");
//    return 1;	 
//}
//	}

//lifo reading 0/p is child met method called
//rules
//method name cannot be changed
//parameter cannot be changed
//return type cannot be changed
//you cannot change access specifier.It can be increased to higher .From protected to public