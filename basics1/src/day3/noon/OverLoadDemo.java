package day3.noon;

public class OverLoadDemo {
	public static void main(String[] args) {
		Help911 usa911help=new Help911();
		
		usa911help.help(new Domestic("brothers are fighting in adjacent house"));
	    usa911help.help(new Theft("There is a theft in my neighbourhood"));
	}

}

class Help911{
	public void help(Theft t) {
		System.out.println("Theft Problem method called..."+t);
		
	}
	public int help(Medical m) {
		System.out.println(m);
		return 1;
	}
	public void help(Domestic d) {
		System.out.println("Domestic problem method called"+d);
		
	}
}
//Rules

//constructor overloading
//name of method should be same
//the return type and access modifier can be different
//parameters should compulsorily be different
class Theft{
String msg;
public Theft(String msg) {
	this.msg=msg;
}
@Override
	public String toString() {
		 
		return "Theft Problem is...:"+this.msg;
	}
}
class Medical{
String msg;
public Medical(String msg) {
	this.msg=msg;
}
@Override
	public String toString() {
		 
		return "Medical Problem is...:"+this.msg;
	}
}
class Domestic{
String msg;
public Domestic(String msg) {
	this.msg=msg;
}
@Override
	public String toString() {
		 
		return "Domestic Problem is...:"+this.msg;
	}
}