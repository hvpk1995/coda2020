package day3.noon;
//polymorphic query
//part whole hierarchy
//composition
//removal of if-else-if
//Object reusability
//code reusability
public class InherDemo {
	public static void main(String[] args) {
		PaintBrush brush=new PaintBrush();
		brush.paint=new PinkPaint();
		brush.doPaint();
	}

}

class PaintBrush{
	 Paint paint;
public void doPaint() {
	System.out.println(paint);
	
}
}

class Paint{}
class RedPaint extends Paint{} 
class BluePaint extends Paint{}
class GreenPaint extends Paint{}
class PinkPaint extends Paint{}
//formula to eliminate if-else-if ladder
//STRATEGY PATTERN
//1.delete if else if
//2.convert the condition to classes
//3.Group them under a hierarchy
//4.Greate association between using class and top hierarchy class