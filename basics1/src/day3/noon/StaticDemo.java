package day3.noon;

public class StaticDemo {
public static void main(String[] args) {
	SingleTon st1= SingleTon.createSingleTon();
	 
	SingleTon st2 =SingleTon.createSingleTon();
	MultiTon mt1=new MultiTon();
	MultiTon mt2=new MultiTon();
	
}
}
class SingleTon{
	private SingleTon() {
		System.out.println("Singleton Object Created..");
	}
	private static SingleTon single;//single is static variable
	public static SingleTon createSingleTon() {
		if(single==null) {
			single=new SingleTon();
		}
		 
			return single;
		
		 
	}
}

class MultiTon{
	public MultiTon() {
		System.out.println("Multiton Object CCreated..");
	}
}