package day3.morning;

public class AccessDemo {//outer class can be either public or no modifier
//public-global access class,sub class,associated class in same package or different package.
	//protected-class,subclass,associated class and associated class in different package
	//no modifier-class,sub class,sub class in same package
	//private-class
	private class A{
		
	}//inner class(a class within a class) can be either public/protected/private/nomod
	public int pub;
	int nomod;
	protected int pro;
	private int pri;
	//experimenting within a class
	public void met() {
		System.out.println(pub);
		System.out.println(nomod);
		System.out.println(pro);
		System.out.println(pri);
	}
}
//experimenting with sub class in same package
class Sub extends AccessDemo{//extends-generaliztion
	public void met() {
		System.out.println(pub);
		System.out.println(nomod);
		System.out.println(pro);
		//System.out.println(pri);//-private variable not available
	}
	
}
class Nonsub{
	public void met() {
		AccessDemo obj=new AccessDemo();//associaton
		System.out.println(obj.pub);
		System.out.println(obj.nomod);
		System.out.println(obj.pro);
		//.out.println(obj.pri);//private variable not available
	}
	
}