package day3.morning;

public class ClassObjectDemo {
	public static void main(String[] args) {
		Bank mannarbank=new Bank();
		mannarbank.depositWork(mannarbank.rajni, mannarbank.c[1]);
	}
}
class Bank{
	String name="mannar and mannar bank.....";//single data....
	String activities[]= {"deposit","withdraw","loans"};//multiple data
	BankManager kamal=new BankManager();//complex type - data(single/multiple) + behaviour
	BankManager rajni=new BankManager();
	Customer ramu=new Customer();
	Customer somu=new Customer();
	Customer c[]= {ramu,somu};
	
	public void depositWork(BankManager manager,Customer c) {
		manager.vetti();
		c.punnagaiMannar();
	}
}
class BankManager{
	String name="kaipulla";
	public void vetti() {
		System.out.println("vaddii.......");
	}
}
class Customer{
	int accid;
	int cash;
	public void punnagaiMannar() {
		System.out.println("deposit cash....and pay interest.....");
	}
}