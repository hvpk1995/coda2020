package day3.morning;

public class ConsDemo {
	public static void main(String[] args) {
		//constructor should be name of class
		Human shoaib=new Human();
		new Human("aaaa"); //parameter constructor-virtual method invocation-polymorphism
		new Human(123);
		new Human();//non parameter constructor
		//System.out.println(shoaib);
	}

}
class Human
{
	public Human() {
		this("aaaa");
		System.out.println("no parameters.");
	}
	public Human(String s) {
		System.out.println("with parameters.");
	}
	
	public Human(int i) {
		System.out.println("number constructor called..");
	}
	@Override
	public String toString() {
		return "Human [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
//@Override
//public String toString() {
//	// TODO Auto-generated method stub
//	return   "Human Object..and hashcode"+this.hashCode();
//	//pom id pill off material
//	//this refer to current object
//	//this refers to current class which is associated
//}

}