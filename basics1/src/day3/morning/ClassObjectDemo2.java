package day3.morning;

public class ClassObjectDemo2 {
public static void main (String[] args) {
	int value=1000;
	System.out.println("Value ...:"+value);
	PBV pbv=new PBV();
	pbv.acceptValue(value);
	System.out.println("Value after Passing..:"+value);
	/*When u pass a primitive type,
	only value is passed and not the reference*/
	PBR pbr =new PBR();
	//when u pass a complex type object is passed and not just value
	InkPen pen=new InkPen();
	System.out.println("Ink Condition..."+pen.ink);
	pbr.acceptObject(pen);
	System.out.println("Ink Condition after passing..."+pen.ink);
  }
}
class PBV{
	public void acceptValue(int value) {
		value=0;
	}
}
class PBR{
	public void acceptObject(InkPen pen) {
		pen.ink="half..";
	}
}
 
class InkPen{
	  
	String ink="full";
}