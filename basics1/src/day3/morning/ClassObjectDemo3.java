package day3.morning;

public class ClassObjectDemo3 {
	public static void main (String[] args) {
		ReservationCounter central=new ReservationCounter();
		Customer1 ramu=new Customer1();
		System.out.println("Money with ramu before Booking....."+ramu.money);
		central.bookTicket(new ReservationSlip(),ramu);
		System.out.println("Money with ramu after booking...."+ramu.money);
	}

}

class ReservationCounter{
	public void bookTicket(ReservationSlip slip,Customer1 customer) {
    System.out.println(slip+"    ticket booking for this slip..."+customer.money+"  is the money given");
	customer.money=customer.money-500;
	}
}

class Customer1{
	int money=1000;
}

class ReservationSlip{
	
}