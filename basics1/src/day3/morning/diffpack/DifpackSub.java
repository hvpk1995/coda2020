package day3.morning.diffpack;

import day3.morning.AccessDemo;

public class DifpackSub extends AccessDemo{
	public void met() {
		System.out.println(pub);
	//	System.out.println(nomod);
		System.out.println(pro);
		//System.out.println(pri);
	}
}

class DifPackNonSub {
	public void met() {
		AccessDemo obj=new AccessDemo();//associaton
		System.out.println(obj.pub);
	//	System.out.println(obj.nomod);
		//System.out.println(obj.pro);
		//System.out.println(obj.pri);
}
}

//sameClass-private,nomod,protected,public
//same Package sub class-no mod,protected,public
//same package non-sub class-nomod,protected,public
//different package sub class-protected,public
//different package non sub class-public