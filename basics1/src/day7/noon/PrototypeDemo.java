package day7.noon;

public class PrototypeDemo {
public static void main(String[] args) {
	//first scenario
	System.out.println("First Scenario-2 objects");
	Sheep sheep1=new Sheep();
	Sheep sheep2=new Sheep();
	sheep1.name="Mother";
	sheep2.name="duplicate";
	//two objects refered by two reference
	System.out.println(sheep1.name+" :" +sheep2.name);
//second scenario
	System.out.println("Second Scenario-1 objects two reference");
	Sheep s1=new Sheep();
	Sheep s2=s1;
	s1.name="mother mother";
	s2.name="dup dup";
	System.out.println( s1.name+" :"+s2.name);
	//third scenario
	
	//prototype pattern-object management
	System.out.println("Third Scenario-Clone/Proto -Resources are shared but properties are unique..one object with two references but data is unique");

	Sheep mothersheep=new Sheep();
	Sheep dolly=mothersheep.createProto();
	mothersheep.name="Iam the mother sheep";
	dolly.name="I am dolly the clone";
	System.out.println(mothersheep.name+":"+dolly.name);
	
}
}
//marker interface
class Sheep implements Cloneable{
	//prototype is clone.
	//shallow copy
	//resources are shared but properties are unique
	String name;
	public Sheep() {
		System.out.println("Sheep cons called");
	}
	public Sheep createProto() {
		try {
			return (Sheep)super.clone();
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
