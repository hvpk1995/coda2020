package day7.noon;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
public class GCDemo {
 public static void main(String[] args) {
	 //Object management using garbage collection
//runtime in lang package
//Runtime constructor cannot be accessed

	 Runtime r=Runtime.getRuntime();
	 System.out.println("Before Thathas Birth  "+r.freeMemory());
     GrandFather tatha=new GrandFather();
     System.out.println("After Thathas Birth.."+r.freeMemory());
     //tatha goes to kasi
    // SoftReference<GrandFather> soft=new SoftReference<GrandFather>(tatha);
  WeakReference<GrandFather> weak =new WeakReference<GrandFather>(tatha);
   //  tatha=null;
     System.out.println("After tathas Death   "+r.freeMemory());
    System.out.println("We will do kariyam");
    r.gc();  //object get deleted permanently
    
    //in reality u dont call gc,it is called when it is needed by jvm-automatic
    
    
    System.out.println("After Kariyam   "+r.freeMemory());
   // tatha=soft.get();
    tatha=weak.get();
    System.out.println(tatha.age);
 }
}
//Garbage collection
//when an object is created memory gets cleaned automatically


class GrandFather{
	String age;
	private String gold="Under the Tree";
	public GrandFather() {
	for (int i = 0; i <1000; i++) {
		age=new String(i+ " ");
	}
	}
	private String getGold() {
		return "The Gold is ...  "+gold;
	}
	 
 	@Override
	//ensures proper closure
	
	//final method cannot be overridden
	//final class cannot be inherited
	//finally block in exceptions always called but not during garbage collection
	protected void finalize() throws Throwable {
		 System.out.println("Finalizeed called.."+getGold());
	}
}