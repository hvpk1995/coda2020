package day7.practice;

 

public class InterfaceSubjectionPractice {
	AlopathyMedicalCollege stanley=new AlopathyMedicalCollege();
	AyurvedaMedicalCollege ayush=new AyurvedaMedicalCollege();
	JetAcademy jet=new JetAcademy();
}
interface Doctor{
	public void doCure();
	public void doGiveMedicine();
}
interface Nurse{
	public void nursing();
}
interface Pilot{
	public void fly();
}
interface Steward{
	public void serve();
}
class AlopathyMedicalCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		System.out.println("alopathy cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("no medicine in alopathy so give inji morabba....");
	}
	@Override
	public void nursing() {
		System.out.println("nursing course...........nurse work done...");
	}
}
class AyurvedaMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("ayurved cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("kabasura neer given to cure.......");
	}
}
class JetAcademy implements Pilot,Steward{
	@Override
	public void fly() {
		System.out.println("learning to fly aeroplane............");
	}
	@Override
	public void serve() {
		System.out.println("become a aeroplane velakaaran....");		
	}
}
class Human{
	
}