package day7.practice;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassPractice {
public static void main(String[] args) throws Exception {
	People pe=new People();
	BankCashier b2=new BankCashier();
	BankManager b3=new BankManager();
	pe.service(b2);
}
}
class People{
	public void service(Object p) throws Exception {
		Class c = p.getClass();
		Field field = c.getField("name");
 		System.out.println(field.get(p));
		Field fields[] = c.getFields();
		for (Field f : fields) {
			System.out.println(f.getName());
		}
		Method met = c.getMethod("work", new Class[] { String.class });
		met.invoke(p, new Object[] { "hello world" });
		Method methods[] = c.getMethods();
		for (Method m : methods) {
			System.out.println(m.getName());
		}
		secretMeeting(p);
	}
	public void secretMeeting(Object p) throws Exception{
		Class c = p.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(p));

		Method met = c.getDeclaredMethod("work", new Class[] { String.class });
		met.setAccessible(true);
		met.invoke(p, new Object[] { "aaaaa" });
	}
	
}
class BankCashier{
	public String name="I handle cash deposits";
	public void work(String s) {
		System.out.println("The work is.."+s);
	}
}
class BankManager{
	public String name="I am in charge of bank";
	private String secretName="I have access to locker and user databases";
	public void work(String name) {
		System.out.println("I do social work ");
		
	}
	private void secretWork(String name) {
		System.out.println("Know details of customer accounts"+name);
	}
}
