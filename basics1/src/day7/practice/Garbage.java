package day7.practice;

import java.lang.ref.WeakReference;

public class Garbage {
	public static void main(String[] args) {
		Runtime r=Runtime.getRuntime();
		System.out.println("before birth "+r.freeMemory());
	    GrandFather tt=new GrandFather();
	    System.out.println("after birth "+r.freeMemory());
	    tt=null;
	    r.gc();
	    System.out.println("After death"+r.freeMemory());
	    WeakReference<GrandFather> weak=new WeakReference<GrandFather>(tt);
	    tt=weak.get();
	}

}
class GrandFather{
	String age;
	private String secret="Under the tree";
	public GrandFather() {
	
	for(int i=0;i<9999;i++) {
		age=new String(i+"");
	}
	}
	private String getGold() {
		return "The gold is"+secret;
	}
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Finalized called"+getGold());
}
}