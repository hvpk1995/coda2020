package day7.morning;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassDemo {
	public static void main(String[] args) throws Exception {
		PoliceStation p1 = new PoliceStation();
		Politician ramu = new Politician();
		Naxalite nax = new Naxalite();
		p1.arrest(ramu);
//	p1.arrest(nax);
	}
}

class PoliceStation {
	public void arrest(Object p) throws Exception {
//		if(p instanceof Politician) {
//		Politician pol=(Politician)p;
//		}else if(p instanceof Naxalite) {
//			Naxalite nax=(Naxalite)p;
//			System.out.println(nax.name);
// Introspection-we get object but we dont know what it is
// reflection
		Class c = p.getClass();
		Field field = c.getField("name");
// Field field=c.getField("name");
		System.out.println(field.get(p));
		Field fields[] = c.getFields();
		for (Field f : fields) {
			System.out.println(f.getName());
		}
// Method met=c.getMethod("work");
// with args
		Method met = c.getMethod("work", new Class[] { String.class });
		// met.invoke(p);
		met.invoke(p, new Object[] { "hello world" });
		Method methods[] = c.getMethods();
		for (Method m : methods) {
			System.out.println(m.getName());
		}
		tortureRoom(p);
	}

	public void tortureRoom(Object p) throws Exception {
		Class c = p.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(p));

		Method met = c.getDeclaredMethod("secretWork", new Class[] { String.class });
		met.setAccessible(true);
		met.invoke(p, new Object[] { "aaaaa" });

	}
// we dont need to cast a class to a type
// without casting itself we can read fields and methods
}

//encapsulation is binding data with behaviour
//hiding can be broken 

class Naxalite {
	public String name = "I am naxalite";

	public void work(String s) {
		System.out.println("The work is.." + s);
	}

}

class Politician {

	public String name = "Iam a Good Holy Man";
	private String secretName = "Gunda..rowdy..dash dash";

	public void work(String name) {
		System.out.println("I do social work.very good person.iam noble.iam for people");

	}

	private void secretWork(String name) {
		System.out.println("murder..dacoit..loot..arson.dash dash" + name);
	}

}
