//package day7.morning;
//public class FieldMethodClassDemo1 {
//	public static void main(String[] args) {
//		PoliceStation p1=new PoliceStation();
//		Politician ramu=new Politician();
//		Naxalite nax=new Naxalite();
//		p1.arrest(ramu);
//	}
//}
//class PoliceStation{
//	public void arrest(Human p) {
//			p.visit();
//	}
//}
//abstract class Human{
//	abstract public void visit();
//}
//class Process{
//	public void handle(Politician p) {
//		p.work();
//	}
//	public void handle(Naxalite n) {
//		System.out.println(n.name);
//	}
//}
//class Naxalite extends Human{
//	public String name="i am naxalite..";
//	@Override
//	public void visit() {
//		new Process().handle(this);
//	}
//}
//class Politician extends Human{
//	public String name="I am Good Holy Man";
//	private String secretName="gunda...rowdy...dash dash...";
//	
//	public void work() {
//		System.out.println("I do social work.....very good person...I am noble....I am for people and .....");
//	}
//	
//	private void secretWork() {
//		System.out.println("murder....dacoit....loot...arson....dash dash.......");
//	}
//	@Override
//	public void visit() {
//		new Process().handle(this);
//	}
//}