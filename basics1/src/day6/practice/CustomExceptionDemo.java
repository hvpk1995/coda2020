package day6.practice;

import java.util.Scanner;

 
public class CustomExceptionDemo {
	public static void main(String[] args) {
		Child child=new Child();
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter a item");
		String item=scan.next();//waiting
		
		Dog dog=new Dog();
		child.playWithDog(item, dog);
	}
}
class Child{
	public void playWithDog(String item,Dog dog) {
		try {
			dog.play(item);
		}catch(DogExceptions de) {
			de.visit();
		}
	}
}
class Dog{
	public void play(String item) throws DogExceptions {
		if(item.equals("stick")) {
			throw new DogBiteException();
		}else if(item.equals("stone"))
		{
			throw new DogBarkException();
		}
		else if(item.equals("biscuit")) {
			throw new DogHappyException();
			
		} 
	}
}
  abstract class DogExceptions extends Exception {
public abstract void visit();
}
class DogBiteException extends DogExceptions{
	@Override
	public void visit() {
	new Handle911().handle(this);	
	}
}
class DogBarkException extends DogExceptions{
	@Override
	public void visit() {
		new Handle911().handle(this);	
		
	}
}
class DogHappyException extends DogExceptions{
@Override
	public void visit() {
	new Handle911().handle(this);	
		
	}	
}
  class Handle911 {
public void handle(DogBiteException dbe) {
	System.out.println("takehim to hospital");
}
public void handle(DogBarkException dbe) {
	System.out.println("takehim to home");
}
public void handle(DogHappyException dbe) {
	System.out.println("yummy yummy..lets play with dog");
}
}

