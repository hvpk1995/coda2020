package day6.noon;

public class InterfaceDemo {
//public wants doctor D.Meghana to cure them
public static void main(String[] args) {
	//Doctor d=new Meghana();
	//d.doCure();
}
}

//Interfaces are special classes whose activity is promised by class which implements it
//when an object is subjected to implementation the object becomes a component
//the using objects Realize the objective of interface through the object which was subjected
class Meghana{
	//she wants to be a doctor
}

interface Doctor
{
	public void doCure();
}

class AlopathyMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("Alopathy implementation logic for doctor written here..");
	}
}

class HomeoMedicalCollege implements Doctor{
	public void doCure() {
		System.out.println("Homeo Implementation logic for doctor written heres");
	}
}