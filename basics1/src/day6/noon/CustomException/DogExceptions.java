package day6.noon.CustomException;

public abstract class DogExceptions extends Exception {
public abstract void visit();
}
class DogBiteException extends DogExceptions{
	@Override
	public void visit() {
	new Handle911().handle(this);	
	}
}
class DogBarkException extends DogExceptions{
	@Override
	public void visit() {
		new Handle911().handle(this);	
		
	}
}
class DogHappyException extends DogExceptions{
@Override
	public void visit() {
	new Handle911().handle(this);	
		
	}	
}