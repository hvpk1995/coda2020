package day6.noon.CustomException;

public class Dog {
public void play(String item) throws DogExceptions {
	if(item.equals("stick")) {
		throw new DogBiteException();
	}else if(item.equals("stone"))
	{
		throw new DogBarkException();
	}
	else if(item.equals("biscuit")) {
		throw new DogHappyException();
		
	} 
}
}
