package day6.morning;

import java.util.Scanner;

public class CustomExceptionDemo {
public static void main(String[] args)    {
TestCustomException te=new   TestCustomException();
try {
	te.test();
}
catch (Throwable t) {
	System.out.println(t);

}
}
}
class TestCustomException {
	public void test()throws Throwable {
		//1.throws skips the compile time check for exception handling
		//2.it conveys to the caller that this method is capable of throwing an exception
		//3.it ensures that the calling method handles or throws the exception
		//4.this scenario is called as handling checked exception

		 
			Scanner scan=new Scanner(System.in);
			System.out.println("Input Some value");
			String in=scan.next();
			if(in.equals("Ramu")) {
				//try {
				throw new MyException("This is custom exception");//throw is to throw expception
			//}
//				catch(Throwable e) {
					
				//}
		}
	
}
}
class MyException extends Throwable{
	String msg;
	public MyException(String msg) {
		this.msg=msg;
	}
 @Override
public String toString() {
	// TODO Auto-generated method stub
	return "The Exception message is..."+msg;
}
}