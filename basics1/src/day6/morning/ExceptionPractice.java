package day6.morning;

public class ExceptionPractice {
public static void main(String[] args)   {
	System.out.println("Before Exception");
	try {
	int i=1/Integer.parseInt(args[0]) ;
	
}
	catch(ArithmeticException e){
		System.out.println(e);
		
	}
	catch(ArrayIndexOutOfBoundsException ae) {
		System.out.println(ae);
	}
	 finally {
		 System.out.println("Finally Block Executed");
	 }
	System.out.println("After Exception");
}
}