package day6.morning;

public class ExceptionDemo3 {
public static void main(String[] args) {
	System.out.println("Before Exception");
	try {
	int i=1/Integer.parseInt(args[0]);// abnormal condition
	}
	catch(ArithmeticException e) {
		System.out.println(e);
	}
	catch(ArrayIndexOutOfBoundsException ae){
		System.out.println(ae);
	}catch(NumberFormatException ne) {
		System.out.println(ne);
	}finally {
		System.out.println("Finally Called");
	}//finally is safety block .It is always executed
	System.out.println("After Exception");
	//exception handling ensures program is not stopped abruptly
	//robust language
	
}
}
//two types of exception
//1.system exception eg:arrayindexout of bound {try multiple catch}
//-jvm itself will throw exception
//only 10% system exception
//2.custom exception (multiple catch should not be used)
//we can have multiple try catch
// we can have try within a try