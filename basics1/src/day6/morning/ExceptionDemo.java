package day6.morning;

public class ExceptionDemo {
public static void main(String[] args) {
	System.out.println("Before Exception");
	try {
	int i=1/Integer.parseInt(args[0]);// abnormal condition
	}
	catch(Exception e) {
		// handle logic
		if (e instanceof ArrayIndexOutOfBoundsException) {
		System.out.println("Array Exception handled");
		
	}
		else if(e instanceof NumberFormatException) {
			System.out.println("Number Format Handled");
		}
		else if(e instanceof ArithmeticException) {
			System.out.println("Arithmetic Problm Handled");
		}
	
	System.out.println("After Exception");
	//exception handling ensures program is not stopped abruptly
	//robust language
	
}
}
}
