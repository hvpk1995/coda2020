package day6.morning;

public class ExceptionDemo2 {
public static void main(String[] args) {
	System.out.println("Before Exception");
	try {
	int i=1/0;// abnormal condition
	}
	catch(Exception e) {
		//action-handling code (but we wrote printing code)
		System.out.println(e);
		
	}
	System.out.println("After Exception");
	//exception handling ensures program is not stopped abruptly
	//robust language
	
}
}
