package day6.morning;

 
public class CorruptionDemo {
public static void main(String[] args) {
	//how public is doing
//	Corporation c=new Corporation();
//	Hospital h=new Hospital();
//	Police p=new Police();
//	
//	p.doInvestigation();
//	h.doPostMortem();
//	c.deathCertificate();

	//initializers
	ESeva eseva=new ESeva();
eseva.setCommand(1, new DeathCertificateCommand());
//how public is doing
eseva.runCommand(1);

}
}
class ESeva{
	Command c[]=new Command[5];
	public ESeva() {
		for(int i=0;i<5;i++) {
			c[i]=new DummyCommand();
		}
	}
	public void setCommand(int slot,Command command) {
		c[slot]=command;
	}
	public void runCommand(int slot) {
		c[slot].execute();
	}
}
abstract class Command{
	private Corporation c ; 
	private Hospital h;
	private Police p;
	public Command() {
		c=new Corporation();
		h=new Hospital();
		p=new Police();
	} 
	public Corporation getC() {
		return c;
	}
	protected abstract void execute();
	public void setC(Corporation c) {
		this.c = c;
	}
	public Hospital getH() {
		return h;
	}
	public void setH(Hospital h) {
		this.h = h;
	}
	public Police getP() {
		return p;
	}
	public void setP(Police p) {
		this.p = p;
	}


}
class DummyCommand extends Command{

	@Override
	protected void execute() {
		 
			System.out.println("I am dummy yet to be operational....");
		 	
	}
	 
}
 class DeathCertificateCommand extends Command{
	public void execute() {
		System.out.println("Death certificate process started by death command");
	    
	}
}
class Corporation{
	public void deathCertificate() {
		System.out.println("Death Certificate Givven");
	}
}
class Hospital{
	public void doPostMortem() {
		System.out.println("Do Postmortem");
	}
}
class Police{
	public void doInvestigation() {
		System.out.println("Police doing investigation");
	}
}
