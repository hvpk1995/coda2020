package day4.practice;

import java.util.Scanner;

 
public class BadProgram {
	public static void main(String[] args) throws Exception{
	Dog tiger=new Dog();
	Child baby=new Child();
	Scanner scan=new Scanner(System.in);
	System.out.println("Please enter a item name..");
	String itemclass=scan.next();
	Item item=(Item)Class.forName(itemclass).newInstance();
	baby.playWithDog(tiger,item);
}
}
class Dog{
	public void play(Item item) {
		item.execute();
	}
}

abstract class Item{
	abstract void execute();
}

class Stick extends Item{

	@Override
	void execute() {
System.out.println("Hit with stick");		
	}
	
}
class Stone extends Item{

	@Override
	void execute() {
		System.out.println("Hit with stone");		
		
	}
	
}
class Child{
	public void playWithDog(Dog dog,Item item) {
		dog.play(item);
}
}