package day4.practice;

import java.util.Scanner;

public class BadProgram2 {
public static void main(String[] args)    {
	 
	GoodFan khaitan=new GoodFan();
	Scanner scan=new Scanner(System.in);
	
	 
	while(true) {
		System.out.println("Enter to call pull method...");
		scan.next ();
		 khaitan.pull();
	}
	
}
}

//bidirectional association
//exercise on object state management
 class GoodFan{
	 State state =new SwitchOffState();
	 public void pull() {
		 state.execute(this);
	 }
 }
abstract class State{
	public abstract void execute(GoodFan f) ;
}

 
class SwitchOffState extends State{
		public void execute(GoodFan f) {
		System.out.println("Switch  on state..");
		f.state=new SwitchOnState();
	}
}

class SwitchOnState extends State{
	public void execute(GoodFan f) {
	System.out.println("Switch  on state..");
	f.state=new SwitchOffState();
}
}