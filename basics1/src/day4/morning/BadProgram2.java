package day4.morning;

import java.util.Scanner;

public class BadProgram2 {
public static void main(String[] args)    {
	//BadFan shaitan=new BadFan();
	GoodFan khaitan=new GoodFan();
	Scanner scan=new Scanner(System.in);
	
	 
	while(true) {
		System.out.println("Enter to call pull method...");
		scan.next ();
		//shaitan.pull();
		khaitan.pull();
	}
	
}
}
//class BadFan{
//	int state=0;
//	public void pull() {
//		if(state==0) {
//			System.out.println("Switch on state..");
//		    state=1;
//		}
//		else if(state==1) {
//			System.out.println("Medium Speed State");
//			state=2;
//		}
//		else if(state==2) {
//			System.out.println("High speed State");
//			state=3;
//		}
//		else if(state==3) {
//			System.out.println("Switch off State");
//		  state=0;
//		}
//	}
//}
//bidirectional association
//exercise on object state management
 class GoodFan{
	 State state =new SwitchOffState();
	 public void pull() {
		 state.execute(this);
	 }
 }
abstract class State{
	public abstract void execute(GoodFan f) ;
}

 
class SwitchOffState extends State{
		public void execute(GoodFan f) {
		System.out.println("Switch  on state..");
		f.state=new SwitchOnState();
	}
}
class SwitchOnState extends State{
	public void execute(GoodFan f) {
	System.out.println("Medium state..");
	f.state=new MediumSpeedState();
}
}
class MediumSpeedState extends State{
	public void execute(GoodFan f) {
	System.out.println("High speed state..");
	f.state=new HighSpeedState();
}
}

class HighSpeedState extends State{
	public void execute(GoodFan f) {
	System.out.println("Switch  off state..");
	f.state=new SwitchOffState();
}
}