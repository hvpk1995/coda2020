package day4.morning;

public class AggregateDemo {
public static void main(String[] args) {
	ShakthiSocket ss=new ShakthiSocket();
	//ShakthiPlug sp= new ShakthiPlug();
	HPPlug hp=new HPPlug();
	IndianAdapter ia=new IndianAdapter();
	ia.ap=hp;
	ss.roundPinHole(ia);
}
}

abstract class IndianPlug{
	public abstract void roundPin();
}
class ShakthiPlug extends IndianPlug{
	@Override
	public void roundPin() {
		System.out.println("Indian Plug Working");
	}
}

abstract class IndianSocket{
	public abstract void roundPinHole(IndianPlug ip);
}

class ShakthiSocket extends IndianSocket{
@Override
public void roundPinHole(IndianPlug ip) {
	ip.roundPin();
}
}
abstract class AmericanPlug{
	public abstract void slabPin();
}

class HPPlug extends AmericanPlug{

	@Override
	public void slabPin() {
		// TODO Auto-generated method stub
		System.out.println("American Slab Pin Working");
	}
	
}
class IndianAdapter extends IndianPlug{
	AmericanPlug ap;
	public void roundPin() {
		ap.slabPin();
	}
}