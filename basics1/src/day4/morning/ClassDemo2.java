package day4.morning;

import java.lang.reflect.Constructor;

public class ClassDemo2 {
public static void main(String[] args) throws Exception{
	//TestParent t=new Test1();//Object is created-static way
	//Class.forName("day4.morning.Test1");
	//Class c=Class.forName("day4.morning.Test1");
	//TestParent o2=(TestParent)c.newInstance();
 //(without parameter)TestParent o2=(TestParent)Class.forName("day4.morning.Test1").newInstance()	;
Constructor cons=Class.forName("day4.morning.Test1").getConstructor(String.class);
TestParent o2=(TestParent)cons.newInstance("bla bla");
 o2.work();
 
 
}
}
class TestParent{
	public void work() {
		
	}
}
class Test1 extends TestParent{
	public Test1(String name) {
		System.out.println("Test Object Created....");
	}
	public void work() {
		System.out.println("Work Method of Test Called..");
	}
}

class Test2 extends TestParent{
	@Override
	public void work() {
		System.out.println("Work Method of Test2 Called..");
	}
}