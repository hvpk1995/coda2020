package day4.morning;

import java.util.Scanner;

public class BadProgram {
	public static void main(String[] args) throws Exception {
		Dog tiger=new Dog();
		Child baby=new Child();
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter a item name..");
		String itemclass=scan.next();
		Item item=(Item)Class.forName(itemclass).newInstance();
		baby.playWithDog(tiger,item);
	}

}

class Dog{
	public void play(Item item) {
//		if(item.equals("stick"))
//				{
//			System.out.println("You beat I bite..");
//			
//				}
//		else if(item.equals("stone")) {
//			System.out.println("You hit I");
//		}
	
	//the store can go on as we cant determine the conditions which are possible
	 item.execute();
	}
}
//abstract may/may not have a behaviour
abstract class Item{public abstract void execute();}
class Stick extends Item{
	@Override
	public void execute(){
		System.out.println("You beat I bite..");
	}
}
class Stone extends Item{
	@Override
	public void execute(){
		System.out.println("You hit I  ..");
	}
}

class Child{
	
	public void playWithDog(Dog dog,Item item) {
		dog.play(item);
	}
}