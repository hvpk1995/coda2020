package day4.morning;

public class ClassDemo {
   public static void main(String[] args) throws Exception {
	Test t=new Test();//static way of creating object
	Class c=Class.forName("day4.morning.Test");
	Object o=c.newInstance();
	((Test)o).work();
}
}

class Test{
	public Test() {
		System.out.println("Test object created..");
		
	}
	public void work() {
		System.out.println("Work method of test called");
	}
}