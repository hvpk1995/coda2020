package day11.morning.treeset;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ColDemo3 {
	public static void main(String[] args) {
	Set <String> st=new TreeSet<String>();
	Set<String> sttt=new TreeSet<>(new Comparator<String>() {
		@Override
		public int compare(String o1, String o2) {
		return o2.compareTo(o1);
		}
	});
	Set <String> stt=new TreeSet<>((String o1,String o2)->{return o1.compareTo(o2);});
st.add("aaaa");
st.add("bbb");
st.add("aaa");
st.add("zzz");
st.add("ee");
//treeset retrieval is faster as it is a sorted collection
System.out.println(st.size());
	System.out.println(st);
	for(String s:st) {
		System.out.println(s);
	}
	Iterator<String> iter=st.iterator();
	while(iter.hasNext()) {
		System.out.println(iter.next());
	}

Set<Employee> ste=new TreeSet<Employee>(new MyComparator());
ste.add(new Employee("ramu",20));
ste.add(new Employee("somuu",30));
System.out.println(ste);
for(Employee s:ste) {
	System.out.println(ste);
}
Iterator<Employee> iter1=ste.iterator();
while(iter1.hasNext()) {
	System.out.println(iter1.next());
}
}
}
class MyComparator implements Comparator<Employee>{

@Override
public int compare(Employee o1, Employee o2) {
return o1.compareTo(o2);	 
}
}


//when we add an element comparator will start working
 

class Employee implements Comparable<Employee>{
	String name;
	Integer age;
	public Employee(String name, int age) {
		 
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
	 return name+":"+age;
	}
	@Override
	public int compareTo(Employee o) {
		return this.age.compareTo(o.age);
	}
	 
}