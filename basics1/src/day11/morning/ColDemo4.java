package day11.morning;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.*;
 
 
 public class ColDemo4 {
public static void main(String[] args) {
	Map<String,String> map=new HashMap<String,String>();
	map.put("a", "hello");
	map.put("b", " ello-hai");
	map.put("c","hello-hai-hello");
	
	System.out.println(map);
	System.out.println(map.get("b"));
	
	//no iterator and list iterator
	//entry set returns an object of set
	//through set we can get iterator
	Set<Map.Entry<String,String>> set=map.entrySet();
	
	Iterator<Map.Entry<String,String>> iter=set.iterator();
	while(iter.hasNext()) {
		Map.Entry<String, String> me=iter.next();
		System.out.println(me.getKey()+":"+me.getValue());
	}


}
}
  