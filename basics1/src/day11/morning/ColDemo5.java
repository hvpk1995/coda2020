package day11.morning;
import java.util.*;
public class ColDemo5 {
public static void main(String[] args) {
	//HashMap<Key,String> wm=new HashMap<Key,String>();
	WeakHashMap<Key,String> wm=new WeakHashMap<Key,String>();
	Key a1=new Key("a1");
	Key a2=new Key("a2");
	wm.put(a1, "hello");
	wm.put(a2, "hai");
	
	System.out.println(wm);
	a1=null;
	System.gc();
	System.out.println(wm);
	//hashmap will not garbage collect
	//if we use weak hash map it will be garbage collected
}
}

class Key{
	String id;
	public Key(String id) {
     this.id=id;
}
}