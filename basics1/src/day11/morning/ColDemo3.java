package day11.morning;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ColDemo3 {
public static void main(String[] args) {
	Set<String> st=new HashSet<String>(10,10);//capacity 10..after 10 another 10 will be allocated
	st.add("aaa");
	st.add("bbb");
	st.add("aaa");
	st.add("zzzz");
	st.add("ee");
	
	System.out.println(st.size());
	//no sequence maintained.no duplicates
	 System.out.println(st);
	//for (String s:st) {
	//	System.out.println(s);
	//}
	
	//Iterator<String> iter=st.iterator();
	//while(iter.hasNext()) {
		//System.out.println(iter.next());
	//}
	//no list iterator
	
	boolean b=st.contains("aaaa");
	System.out.println(b);
	//st.remove("aaa");
}
}
