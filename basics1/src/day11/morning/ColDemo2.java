package day11.morning;

import java.util.Stack;

public class ColDemo2 {
public static void main(String[] args) {
	Stack <String >st=new Stack<>();
	//pop
	//peek
	//search
	st.add("aa");
	st.add("bb");
	st.push("zzz");
	System.out.println(st);
	for (String s:st) {
		System.out.println(s);
	}
	//searching a stack
	System.out.println(" Element available at that position  "+st.search("aa"));
	//should display element on top
System.out.println("Top Element...:"+st.peek());	
	
	//traversing from top
	for (int i = 0; i < 3; i++) {
		System.out.println(st.pop());
	}
	
	//After popping the stack becomes empty..
	System.out.println(st);
	
}
}
