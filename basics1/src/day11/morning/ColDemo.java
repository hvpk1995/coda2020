package day11.morning;

import java.util.ArrayList;
import java.util.List;
import java. util. Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
public class ColDemo {
public static void main(String[] args) {
//	List <String> list=new LinkedList<>();
	List <String> list=new ArrayList<>();
List <String> list2=new LinkedList<>();
	list.add("aaaa");
	list.add("bbbb");
	//array list stores data in sequence.linked list is in network.address is stored in linked list
	//
	System.out.println(list);
	//System.out.println(list.get(0));
	//get(0) gets 0th element
	
	for(int i=0;i<list.size();i++)
	{
		//System.out.println(list.get(i));
	}
	for(String s:list) {
		//System.out.println(s);
		
	}
	
	Iterator <String> iter=list.iterator();
	 //list.add("cccc");
	//cannot modify after using iterator if we use iterator will fail
	//iterator one time reading.not transferrable
	while(iter.hasNext()) {
		 System.out.println(iter.next());
	}
	//list iterator go previous and next
	ListIterator<String> lisiter=list.listIterator();
while(lisiter.hasPrevious())
	{
	System.out.println(lisiter.previous());
	}
}
}
