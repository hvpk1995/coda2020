package day11.morning;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
 

public class ColDemoDictionary {
public static void main(String[] args) {
	 Dictionary<String,String> d = new Hashtable();
//put method
d.put("a", "Code");
d.put("b", "Program");
//elements method
for(Enumeration  i=d.elements();i.hasMoreElements();) {
	System.out.println("Value in Dictionary"+i.nextElement());
}
 //get() Method

System.out.println("Value at key 6"+d.get("6"));
System.out.println("Value at key a"+d.get("a"));
//is empty
System.out.println("There is no key value pair"+d.isEmpty());
//keys method
for(Enumeration k=d.keys();k.hasMoreElements();) {
	System.out.println("Keys in dictionary"+k.nextElement());
}


}
}
