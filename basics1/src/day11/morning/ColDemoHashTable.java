package day11.morning;

import java.util.Hashtable;

public class ColDemoHashTable {
public static void main(String[] args) {
	Hashtable<Integer, String> h = new Hashtable<Integer, String>();
   h.put(1, "aaa");
   h.put(2, "bbb");
   h.put(3, "ccc");
   
   System.out.println(h);
	Hashtable<Integer, String> h1 = new Hashtable<Integer, String>();
//clone method
	h1=(Hashtable<Integer,String>)h.clone();
	System.out.println(h1);
   //clear hashtable
	h.clear();
	System.out.println("After Clearing"+h);
}
}
