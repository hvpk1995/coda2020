package day11.noon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class ObserverDemo {
public static void main(String[] args) {
	FireAlarm sakthi=new FireAlarm();
	Students coda=new Students();
	Teacher shoiab=new Teacher();
	
	sakthi.addObserver(coda);
	sakthi.addObserver(shoiab);
	sakthi.setFire();
}
}

class ThreadedObservable extends Observable{
	ArrayList <Observer> list=new ArrayList<Observer>();
	@Override
	public void notifyObservers(Object arg) {
		  for(Observer o:list) {
			  new Thread(()->{
				  //for every observer new thread is created
			  o.update(this, arg);
			  }).start();
		  }
	}
	@Override
	public synchronized void addObserver(Observer o) {
		list.add(o);
	}
}

class FireAlarm extends ThreadedObservable{
	 
	public void setFire() {
	setChanged();
	notifyObservers("fire in mountain run run run");
	}
}
class Students implements Observer{
	@Override
	public void update(Observable observable,Object msg) {
		System.out.println((String)msg);
		odungo();
		   
		 
		
	}
	public void odungo() {
		System.out.println("Students are running");
	}
}

class Teacher implements Observer{
	@Override
	public void update(Observable o, Object msg) {
		System.out.println((String)msg);
		System.out.println("I have pain,thinking whether I can run or not");
		 
		try {
			 
			 
			Thread.sleep(5000);
		}
		catch(Exception e){}
		parigattu();
		
	}
public void parigattu() {
	System.out.println("Teacher is running");
}


 }
