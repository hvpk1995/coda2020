package day11.noon;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class DateDemo {
public static void main(String[] args) {
	LocalDate ld=LocalDate.now();
	System.out.println("Current Date is.."+ld);
	LocalDate ldw=ld.plus(70,ChronoUnit.HOURS);
   System.out.println(ldw);
   LocalDate lcd=ldw.plus(1,ChronoUnit.DECADES);
 System.out.println(lcd);
 LocalDate lcw=ldw.plus(1,ChronoUnit.MILLENNIA);
 System.out.println(lcw);
 LocalTime lt=LocalTime.now();
 System.out.println(lt);
 
 Duration noofhours=Duration.ofHours(1);
 LocalTime lt2= lt.plus(noofhours);
 System.out.println(lt2);
 
 Duration d=Duration.between(lt, lt2);
 System.out.println("Difference ..."+d);
 
 LocalDateTime ldt=LocalDateTime.now();
 LocalDate ldate=ldt.toLocalDate();
 System.out.println(ldate);
 Month month=ldt.getMonth();
 int day=ldt.getDayOfMonth();
 int hours=ldt.getHour();
 
 System.out.println(month+":"+day+":"+hours);
}
}
