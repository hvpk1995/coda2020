package labBook1;

import java.util.Scanner;

public class Day1 {
	//2 Default Values
	static int a;
	static char b;
	static String c;
	static byte d;
	static boolean e;
	static float f;
	static double g;
	static long h;
	
public static void Average() {
	int n;
	int total=0;
	
    Scanner sc=new Scanner(System.in);

    System.out.println("enter how many numbers to cal  avg");
	   
    n=sc.nextInt();

    int a[]=new int[n];

    System.out.println("enter   "+n+"  numbers");

    for(int i=0;i<n;i++) {
	   a[i]=sc.nextInt();
    }
    for(int i=0;i<n;i++) {
	   total =total+a[i];
    }	    
     System.out.println("average="+total/n);
      
}
	
public static void main(String[] args) {
	//1
	System.out.println("Hello World");
//2
	System.out.println(a);
	System.out.println(b);
	System.out.println(c);
	System.out.println(d);
	System.out.println(e);
	System.out.println(f);
	System.out.println(g);
	System.out.println(h);

	//3 Type Casting and Initialisation
	int aa=1;
	float bb=aa;
	long cc=aa;
	int dd=(int)cc;
	int ee=(int)bb;
	boolean ff=true;
//	int gg=(int)ff;// typecast not possible from boolean to int
//	boolean hh=(float)aa;
	String strnum="1234";
	int numm=Integer.parseInt(strnum);
	System.out.println(numm);
	
	//4
	Scanner sc=new Scanner(System.in);
	
	int i,j=0,flag=0;      
	System.out.println("Enter the number to be checked");
	int prime=sc.nextInt();
	    
	  j=prime/2;      
	  if(prime==0||prime==1){  
	   System.out.println(prime+" is not prime number");      
	  }else{  
	   for(i=2;i<=j;i++){      
	    if(prime%i==0){      
	     System.out.println(prime+" is not prime number");      
	     flag=1;      
	     break;      
	    }      
	   }      
	   if(flag==0)  { System.out.println(prime+" is prime number"); }  
	  }  
	   //9
		    int intArray[] = {1,2,3,4,5};
		     
		  
		     
		    System.out.println();
		     
		    System.out.println("Original Array printed in reverse order:");
		         for(int ii=intArray.length-1;ii>=0;ii--) {
		         System.out.print(intArray[ii] + "  ");
		         }
	//5
				    System.out.println();

		   Average();      
}
}
