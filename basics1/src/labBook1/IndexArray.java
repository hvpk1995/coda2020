package labBook1;

import java.util.Arrays;
import java.util.Scanner;

public class IndexArray {
public static boolean verify;
public static int findElementIndex(int value,int a[]) {
	int index=0;
	for(int i=0;i<a.length;i++) {
		if(a[i]==value)
		{
			index=i;
			verify=true;
		}
	}
	return index;
}
	
	public static void main(String[] args) {
 
	
	Scanner sc=new Scanner(System.in);
 System.out.println("Enter the Length of the array");
	int array_length=sc.nextInt();
	int array[]=new int[array_length];
System.out.println("Enter the array elements");
for(int i=0;i<array_length;i++) {
	array[i]=sc.nextInt();
}
System.out.println("The array elements are"+Arrays.toString(array));
System.out.println("Enter element you want to check index");
int element=sc.nextInt();
int index=findElementIndex(element,array);
if(verify) {
	System.out.println("The index of  "+element+"  is "+index);
	
}
else
{
	System.out.println("Sorry this value does not exist");
}
}
}
