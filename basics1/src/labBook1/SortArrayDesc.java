package labBook1;

public class SortArrayDesc {
public static void main(String[] args) {
	 int [] arr =  {5, 2, 9, 3, 1};     
     int temp = 0;    
     int temp1 = 0;
     
     System.out.println("Elements of original array: ");    
     for (int i = 0; i < arr.length; i++) {     
         System.out.print(arr[i] + " ");    
     }    
     for (int ii = 0; ii < arr.length; ii++) {     
         for (int jj = ii; jj < arr.length; jj++) {     
            if(arr[ii] < arr[jj]) {    
                temp1 = arr[ii];    
                arr[ii] = arr[jj];    
                arr[jj] = temp1;    
            }     
         }     
     }  
     System.out.println();    
     //Descending
     
     System.out.println("Elements of array sorted in descending order: ");    
     for (int ii = 0; ii < arr.length; ii++) {     
         System.out.print(arr[ii] + " ");    
     }
}
}
