package labBook1;

public class Matrix {

    public static void main(String[] args) {
         
        int[][] mytMatrix = { {2, 3, 4,5}, {5, 2, 3,6},{5, 2, 2,6},{5, 2, 3,6} };
         
 
        System.out.println("Sum of two matrices is: ");
        for(int[] row : mytMatrix) {
            for (int column : row) {
                System.out.print(column + "    ");
            }
            System.out.println();
        }
    }
}