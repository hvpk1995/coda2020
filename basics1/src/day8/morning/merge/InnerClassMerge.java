package day8.morning.merge;
//lambdas demo

	// encapsulated code easy to understand and also it is polymorphic
	// class inside a class becomes part of that class

public class InnerClassMerge {
	public static void main(String[] args) {
		Pepsi pepsico = new Pepsi();
		pepsico.makePepsi();
		// KaliMark kali=new KaliMark();
		// kali.makeBovonto();

	}

}

abstract class Cola {// functional abstract class/interface only one method
	public abstract void makeCola();

	// public abstract void work(); //if we have two methods in abstract we cannot
	// use anonymous inner class
}

interface ColaInter {// functional abstract class/interfacce
	public void makeCola();
}

interface ColaInter2 {
	public void makeCola(String s, int i);
}

class Pepsi {
	public void makePepsi() {
		new Cola() {// anonymous inner class
			@Override
			public void makeCola() {
				System.out.println("Cola Made...");
			}
		}.makeCola();
		//we cannot write lambda for abstract classes
		// Lambda Syntax...to implement anonymous inner class
		// interface name/variable /body(object)
		ColaInter c = () -> {
			System.out.println("Cola Made..");
		};

		c.makeCola();

		ColaInter2 c2 = (value, num) -> {
			System.out.println("value is...:" + value);
			System.out.println("num is...:" + num);
		};

		c2.makeCola("aaa", 23);
		Add add = (i, j) -> {
			return i + j;
		};
		addNumber(10, 20, add);
 	}

	public void addNumber(int i, int j, Add add) {
		System.out.println("Addition of i and j is...:" + add.add(i, j));
	}
}
	interface Add {
		public int add(int i, int j);
	}

 