 package day8.morning.innerclass; 
public class InnerClassDemo {
//lambdas
	//encapsulated code easy to understand and also it is polymorphic
	//class inside a class becomes part of that class
public static void main(String[] args) {
	Pepsi pepsico=new Pepsi();
	pepsico.makePepsi();
	KaliMark kali=new KaliMark();
	kali.makeBovonto();
	
}

}

abstract class Cola{//functional abstract class/interface only one method
	public abstract void makeCola();
}


class Pepsi{
	public void makePepsi() {//local inner class
		class CampaCola extends Cola{
			//campacola class can be static or private also .if it is private then kalimark cannot access it
			@Override
			public void makeCola() {
				// TODO Auto-generated method stub
			System.out.println("cola made by campa cola");	
			}
		}
		Cola cola=new CampaCola();
		cola.makeCola();
		System.out.println("Fill in pepsi bottle sell..");
	}
	
  // public Cola trojan() {
	 //  return new CampaCola();
   //}
}

class KaliMark{
	public void makeBovonto() {
		//Cola cola=new CampaCola();
		//Cola cola=new Pepsi().new CampaCola(); //access inner class
		//Pepsi pep=new Pepsi();
		//Cola cola=new Pepsi.CampaCola();//if campa cola is   static
	//	Cola cola=new Pepsi().trojan();
		//cola.makeCola();
		System.out.println("Fill in bovonto bottle sell");
		
	}
}