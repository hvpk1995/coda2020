package day8.morning;

public class StringDemo {
	public static void main(String[] args) {
		Demo1 obj=new Demo1();
		obj.exploreString();
	}

}
class Demo1{
	public void exploreString() {
		String s1="hello";
		String s2=s1+"world";
		
		System.out.println(s2);
		System.out.println(s1);//non mutable object
		
		String str=new String("Hello");
		String str2=new String(str+"world");
		
		System.out.println(str2);
		System.out.println(str);
		
		//equals-compares two string
		System.out.println(str.equals(str2));
		
		System.out.println(str.charAt(2));
		
		char c[]= {'a','c','f'};
		String cstr=new String(c,0,1);
		System.out.println(cstr);
		//concatenate never use string
		//instead make use of StringBuffer or StringBuilder
		//StringBuffer-old class-prior to jddk 5-thread safe-slow
		//StringBuilder-new Class-introduced in jdk 5-non thread safe -fast
		//buffer methods are synchronised
		//two clients cannot talk to string buffer at same time
		StringBuilder sb1=new StringBuilder("hello");//synchronized-sequential-slower-old
		sb1.append("world");
		StringBuffer sbf=new StringBuffer("hello");
		sbf.append("world");
		//String.format("", args)
		
	 String sf=String.format("The String value:...%s and Integer value is...%d","hello world",200);
   System.out.println(sf);
   System.out.printf("The String value...:%s and Integer value is...%d","hello world",200);
	
	System.out.printf("Integer: %d\n",15);
	System.out.printf("Floating point number with 3 decimal digits:%.3f\n",1.21312939123);
	System.out.printf("Flotating Point number with 9 decimal digits:%f\n",1.21312939123);
	System.out.printf("String:%s, integer:%06d, float:%.6f","Hello World",89,9.231435);
	String s=String.format("String : %s, integer: %d, float:%.6f", "Hello World",89,9.23145);
	System.out.println("\n"+s);
	
	System.out.printf("%-12s%-12s%s\n","column 1","column 2","Column 3");
	
	System.out.printf("%-12.5s%s","Hello World","World");
	
	
	}
 
	public void work(int ...a) {//var args
		for(int i:a) {
			System.out.println(i);
		}
		
	 
}
}