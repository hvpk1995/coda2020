package day8.noon;

import java.util.Set;

public class SingleThreadDemo {
public static void main(String[] args) throws Exception{
	Thread t=Thread.currentThread();
	t.setName("ramu");
	t.setPriority(10);
	//priority ranges from 1 to 10
	//10 priority runs first.
	System.out.println(t);
	for (int i = 0; i < 5; i++) {
		System.out.println(i);
		Thread.sleep(1000);// control check value
		//child thread start from main thread.this is called spanning
		//we can kill main thread by using System.exit(1);
	}
//how to span a new thread
	Thread t2=new Thread(()->{System.out.println("Child Thread..");});//using lambdas -first preferred
	Thread t3=new Thread(new MyRunnable());//traditional
	new Thread(new Runnable() {//anonymous inner classes
		@Override
		public void run() {
System.out.println("child thread..");			
		}
	}).run();
//	Set<Thread> threads = Thread.getAllStackTraces().keySet();
//System.out.println(threads); 
}


}
class MyRunnable implements Runnable{
	@Override
	public void run() {
	System.out.println("Child Thread...");	
	}
}