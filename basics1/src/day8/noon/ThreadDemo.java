package day8.noon;

public class ThreadDemo {
public static void main(String[] args) {
Thread t=new Thread(()->{System.out.println("Child Thread Called");}) ;
t.start();
   // new Thread(new MyRunnable2()).start();
	System.out.println("Main THread called");
}
}

//we are not supposed to call run method
//start creates a mechanism which will call run method
//runnable is a job associated with the thread.work you assign a thread

class MyRunnable2 implements Runnable{
	@Override
	public void run() {
	System.out.println("Child Thread...");	
	}
}