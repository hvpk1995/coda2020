package day8.practice;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.omg.CORBA.portable.InvokeHandler;

 
  

public class InterfaceSub {
public static void main(String[] args) {
	
	AllopathyC al=new AllopathyC();
	AyurvedaC ay=new AyurvedaC();
	FlyingAcademy fa=new FlyingAcademy();
	Human  s=new Human();
 	//Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},
		//	new MyInvocationHandler(new Object[] {al,ay}));
	//Pilot pl=(Pilot)object;
	//pl.flying();
//	AllopathyC stanley=new AllopathyC();
//	AyurvedaC ayush=new AyurvedaC();
//	FlyingAcademy jet=new FlyingAcademy();
//	 Human shoiab=new Human();
//	 Doctor doctorshoiab=(Doctor)Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class}, new MyInvocationHandler(new Object[] {stanley}));
//	
////	doctorshoiab.doCure();
//	
//	Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},
//			new MyInvocationHandler(new Object[] {doctorshoiab,jet}));
////	new MyInvocationHandler(new Object[] {stanley,jet}));
//	//Doctor doctorshoiab=(Doctor)object;
//	Pilot pilotshoiab=(Pilot)object;
//	doctorshoiab.doCure();
//	
//	pilotshoiab.flying();	
//	shoiab.sayHello();
	
}
	
 	class MyInvocationHandler implements InvocationHandler{
 		Object obj[];
 		Object o;
 		public MyInvocationHandler(Object obj[]) {
 			this.obj=obj;
 		}
 		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
          
 			 for (int i = 0; i <obj.length; i++) {
			       try {
			    	   o=method.invoke(obj[i],args);
			       }catch(Exception e){
			    	  
			       }
			        
			}
			return o;
		}
 	}
}
interface Doctor{
	public void doCure();
	public void giveMedicine();
}
interface Nurse{
	public void nursing();
}

interface Pilot{
	public void flying();
}

interface Steward{
	public void serve();
}

class AllopathyC implements Doctor,Nurse{
	@Override
	public void doCure() {
	System.out.println("Corona Cure");
		
	}
	@Override
	public void nursing() {
System.out.println("Nurse Training");		
	}
	@Override
	public void giveMedicine() {
System.out.println("Give corona vaccination");		
	}
}

class AyurvedaC implements Doctor{
	@Override
	public void doCure() {
		System.out.println("Kabasura kudineer");
		
	}
	@Override
	public void giveMedicine() {
System.out.println("Ayurveda medicine given");		
	}
}

class FlyingAcademy implements Pilot,Steward{
	@Override
	public void flying() {
System.out.println("Pilot Training");		
	}
	@Override
	public void serve() {
	System.out.println("Server Training");
		
	}
}

class Human{
	public void sayHello() {
		System.out.println("Hi Human");
	}
}